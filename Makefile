SCALAPACK_ROOT=/home/pwu/Downloads/scalapack-2.0.2
SCALAPACK_LIB=${SCALAPACK_ROOT}/libscalapack.a
PBLAS_LIB=${SCALAPACK_ROOT}/
LAPACK_LIB=-llapack
BLAS_LIB=-lblas
MATGEN_LIB=${SCALAPACK_ROOT}/TESTING/LIN/pdmatgen.o ${SCALAPACK_ROOT}/TESTING/LIN/pmatgeninc.o
LIBS=${MATGEN_LIB} ${SCALAPACK_LIB} ${LAPACK_LIB} ${BLAS_LIB}

FC=mpif90
OPT=-O3

all: xpdgetrf xpdpotrf xpdgeqrf


xpdgetrf: xpdgetrf.o pdgetrf.o ckpt.o
	${FC} ${OPT} -o $@ $^ ${LIBS}
	
xpdgeqrf: xpdgeqrf.o pdgeqrf.o ckpt.o
	${FC} ${OPT} -o $@ $^ ${LIBS}
	
xpdpotrf: xpdpotrf.o pdpotrf.o ckpt.o
	${FC} ${OPT} -o $@ $^ ${LIBS}
	
%.o: %.f90
	$(FC) -cpp -c $(FCFLAGS) -o $@ $<
	
	
clean:
	rm xpdgetrf xpdpotrf *.o