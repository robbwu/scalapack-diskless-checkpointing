PROGRAM XPDGETRF

CALL TEST1
CONTAINS
! ---------- TEST 1 ----------------------
!       big random matrix.
! ----------------------------------------

SUBROUTINE TEST1
        IMPLICIT NONE
        INTEGER                 M, N, NB, NPROW, NPCOL, ICTXT, LDA, LDAR, INFO, &
        &                       DESCA(9), I, J, K, LNC, MYROW, MYCOL, LNR, &
        &                       INFO2, LWORK, IA, JA, MA, NA, &
        &                       II, JJ, IAROW, IACOL
        DOUBLE PRECISION, ALLOCATABLE :: A(:,:), AR(:,:), CHKE(:), CHKV(:), &
                                WORK(:), TAU(:)
        DOUBLE PRECISION        OPTSIZE(1)
        INTEGER                 ICEIL, NUMROC
        DOUBLE PRECISION                MINTIME(32), MAXTIME(32), AVGTIME(32)
        CHARACTER(LEN=10)               ARGSTR
        INTEGER, ALLOCATABLE :: IPIV(:)

        IF (IARGC().NE.7) THEN
           PRINT *, 'ARGUMENTS: M, N, NB, NPROW, NPCOL, IA, JA'
           RETURN
        END IF
        CALL GETARG(1,ARGSTR)
        READ(ARGSTR, '(I8)') M
        CALL  GETARG(2, ARGSTR)
        READ(ARGSTR, '(I6)') N
        CALL GETARG(3,ARGSTR)
        READ(ARGSTR, '(I6)') NB
        CALL GETARG(4,ARGSTR)
        READ(ARGSTR, '(I6)') NPROW
        CALL GETARG(5,ARGSTR)
        READ(ARGSTR, '(I6)') NPCOL
        CALL GETARG(6, ARGSTR)
        READ(ARGSTR, '(I8)') IA
        CALL GETARG(7, ARGSTR)
        READ(ARGSTR, '(I8)') JA

        MA = M*5/4
        NA = N*5/4

        CALL BLACS_GET (0, 0, ICTXT)
        CALL BLACS_GRIDINIT (ICTXT, 'R', NPROW, NPCOL)
        CALL BLACS_GRIDINFO (ICTXT, NPROW, NPCOL, MYROW, MYCOL)

        LNR = NUMROC (MA, NB, MYROW, 0, NPROW)
        LNC = NUMROC (NA, NB, MYCOL, 0, NPCOL)
        LDA = LNR

        ALLOCATE( A(LDA, LNC), CHKE(NB), CHKV(NB) )
        ALLOCATE( TAU(MIN(M,N)) )
        ALLOCATE( IPIV(LNR+NB) )
        CALL DESCINIT (DESCA, MA, NA, NB, NB, 0, 0, ICTXT, LDA, INFO)
        CALL PDMATGEN (ICTXT, ' ', 'N', MA, NA, NB, NB, A, LDA, &
                                        0, 0, 100, 0, LNR, 0, LNC, &
                                        MYROW, MYCOL, NPROW, NPCOL)
!       CALL REF_PDGEQRF( M, N, A, IA,JA, DESCA, TAU, OPTSIZE, -1, INFO )

        CALL SLBOOT()
        CALL BLACS_BARRIER (ICTXT, 'ALL')
        CALL SLTIMER(1)
!       CALL REF_PDGEQRF( M, N, A, IA, JA, DESCA, TAU, WORK, LWORK, INFO )
        CALL CKPT_PDGETRF( M, N, A, IA, JA, DESCA, IPIV, INFO )
        CALL SLTIMER(1)

        IF( INFO /= 0 ) THEN
            PRINT *, 'INFO=',INFO
            CALL ABORT
        END IF
        CALL SLCOMBINE( ICTXT, 'A', '>', 'W', 1, 1, MAXTIME)
!        CALL SLCOMBINE( ICTXT, 'A', '<', 'W', 14, 1, MINTIME)
!        CALL SLCOMBINE( ICTXT, 'A', '+', 'W', 14, 1, AVGTIME)
!        AVGTIME = AVGTIME / (NPROW*NPCOL)
        IF( MYROW.EQ.0 .AND. MYCOL.EQ.0 ) THEN
            PRINT *, "M,N,NB,NPROW,NPCOL, IA, JA",M,N,NB,NPROW,NPCOL, IA, JA
            PRINT *, "TIME(s)", MAXTIME( 1 )
            PRINT *, "GFLOPS", 1.0*N*N*(M-N/3.)/MAXTIME( 1 ) * 1.0E-9
            PRINT *, "GFLOPS/core", 1.0*N*N*(M-N/3.)/MAXTIME( 1 ) * 1.0E-9/(NPROW*NPCOL)
!            PRINT *, "Detailed timing statistics: (MIN,AVG,MAX)"
!            PRINT '(A,T20,3F10.3)', "BACKUP", MINTIME(2), AVGTIME(2), MAXTIME(2)
!            PRINT '(A,T20,3F10.3)', "FTPDGETF2", MINTIME(3), AVGTIME(3), MAXTIME(3)
!            PRINT '(A,T20,3F10.3)', "CHK1", MINTIME(4), AVGTIME(4), MAXTIME(4)
!            PRINT '(A,T20,3F10.3)', "RESTORE", MINTIME(5), AVGTIME(5), MAXTIME(5)
!            PRINT '(A,T20,3F10.3)', "PDLASWP LEFT", MINTIME(13), AVGTIME(13), MAXTIME(13)
!            PRINT '(A,T20,3F10.3)', "PDLASWP CHKSUM LEFT", MINTIME(14), AVGTIME(14), MAXTIME(14)
!            PRINT '(A,T20,3F10.3)', "PDLASWP RIGHT", MINTIME(6), AVGTIME(6), MAXTIME(6)
!            PRINT '(A,T20,3F10.3)', "PDLASWP CHKSUM RIGHT", MINTIME(7), AVGTIME(7), MAXTIME(7)
!            PRINT '(A,T20,3F10.3)', "PDTRSM", MINTIME(8), AVGTIME(8), MAXTIME(8)
!            PRINT '(A,T20,3F10.3)', "PDTRSM CHKSUM", MINTIME(9), AVGTIME(9), MAXTIME(9)
!            PRINT '(A,T20,3F10.3)', "CHK2", MINTIME(10), AVGTIME(10), MAXTIME(10)
!            PRINT '(A,T20,3F10.3)', "FTDGEMM", MINTIME(11), AVGTIME(11), MAXTIME(11)
!            PRINT '(A,T20,3F10.3)', "CHK3", MINTIME(12), AVGTIME(12), MAXTIME(12)
        END IF
        CALL INFOG2L( IA+M-1, JA+N-1, DESCA, NPROW, NPCOL, &
                      MYROW, MYCOL, II, JJ, IAROW, IACOL )
        IF( MYROW.EQ.IAROW .AND. MYCOL.EQ.IACOL ) THEN
           PRINT '(A,2I8,A,E12.4)', 'A(',IA+M-1, JA+N-1,')=', A(II, JJ)
        END IF

        CALL BLACS_EXIT(0)
END SUBROUTINE

END PROGRAM
